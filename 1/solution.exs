defmodule Solution do

  import List
  import Float
  import Enum

  def multiples(max, nums) do
    (for n <- nums, do: mul(max, n))
      |> flatten
      |> uniq
      |> reduce(&(&1 + &2))
  end

  defp mul(max, num) do
    for n <- 1..trunc(ceil(max / num) - 1), do: num * n
  end

end
